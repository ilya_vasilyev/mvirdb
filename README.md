# mvirdb

The snapshot for MvirDB - a microbial database of protein toxins, virulence factors and antibiotic resistance genes for bio-defence applications

Official website (dead): http://mvirdb.llnl.gov/
Official website (archived): https://web.archive.org/web/20170121050640/http://mvirdb.llnl.gov/
